var editor = ace.edit("customCss");
editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/css");

jQuery(document).ready(function($){
	var updateCss = function updateCss() {
		$("#tutorial_css").val(editor.getSession().getValue());
	}

	$("#save-custom-css-form").submit(updateCss);
});