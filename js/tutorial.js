jQuery(document).ready(function($) {

	revealPosts();

	/* Ajax function */

	$('.tutorial-load-more:not(.loading)').on('click', function() {
		var that = $(this);
		var page = that.data('page');
		var newPage = page+1;
		var ajaxurl = $(this).data('url');

		that.addClass('loading').find('.text').slideUp(300);
		that.find('.rotate').addClass('spin');

		$.ajax({
			url : ajaxurl,
			type : 'post',
			data : {
				page : page,
				action : 'tutorial_load_more'
			},
			error : function(response) {
				console.log(response);
			},
			success : function(response) {
				setTimeout(function() {
					that.data('page', newPage);
					$('.tutorial-posts-row').append(response);

					that.removeClass('loading').find('.text').slideDown(300);
					that.find('.rotate').removeClass('spin');

					revealPosts();

				}, 1000);
			}
		});
	});

	var last_scroll = 0;

	$(window).scroll(function() {
		var scroll = $(window).scrollTop();

		if (Math.abs(scroll - last_scroll) > $(window).height()*0.1) {
			last_scroll = scroll;

			$('.page-limit').each(function(index) {
				if (isVisible($(this))) {

				}
			});
		}
	});


	/* Helper Functions */
	function revealPosts() {
		var posts = $('article:not(.reveal)');
		var i = 0;
		setInterval(function() {
			if (i >= posts.length) {
				return false;
			}
			else {
				var el = posts[i];
				$(el).addClass("reveal").find('.carousel').carousel(); 
			}	

			i++;
		}, 200)
	}

	function isVisible(element) {
		var scroll_pos = $(window).scrollTop();
		var window_height = $(window).height();
		var el_top = $(element).offset().top;
		var el_height = $(element).height();
		var el_bottom = el_top + el_height;
		return ((el_bottom - el_height*0.25 > scroll_pos) && (el_top < (scroll_pos + 0.5*window_height)));
	}
});