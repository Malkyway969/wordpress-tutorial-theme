<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container tutorial-posts-container">
				<div class="row tutorial-posts-row">
					<?php 
						if (have_posts()):

							echo '<div class="page-limit" data-page="'.get_site_url().'/page">';

							while (have_posts()): the_post();
								// $class = 'reveal';
								// set_query_var('post-class', $class);
								get_template_part('template-parts/content', get_post_format());
							endwhile;

							echo '</div>';
						endif;
					?>
				</div>
			</div>
			<div class="container text-center">
				<a class="btn-tutorial-load tutorial-load-more" data-page="1" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
					<span class="dashicons dashicons-image-rotate rotate"></span> 
					<span class="text">Load More</span>
				</a>
			</div>
		</main>
	</div>

<?php get_footer(); ?>