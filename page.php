<?php 
	$fullwidthoption = get_option('viewwidth_header');
	$tilesaboveoption = get_option('tiles_above');

	if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
	    get_header();
	}
	else {
	    get_header('containedwidth');
	}
?>

<div class="container background-white">
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
			    <?php
				    // TO SHOW THE PAGE CONTENTS
				    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				        <div class="entry-content col-xs-12">
				            <?php the_content(); ?> <!-- Page Content -->
				        </div><!-- .entry-content-page -->

				    <?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
			</div>
			<?php if (isset($fullwidthoption) && ($fullwidthoption == 0)) : ?>
				<div class="row">
					<?php get_footer(); ?>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>

<?php if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
		get_footer();
	}  
?>