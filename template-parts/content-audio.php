<?php

/*
	Audio Post Format
*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 tutorial-format-audio'); ?>>
	<header class="entry-header">
		<?php the_title('<h1 class="entry-title"><a href="'. esc_url(get_permalink()) .'" rel="bookmark">', '</a></h1>'); ?>

		<div class="entry-meta">
			<?php echo tutorial_posted_meta(); ?>
		</div>
	</header>

	<div class="entry-content">
	<?php 
		echo tutorial_get_media(array('audio', 'iframe'));
	?>
	</div>

	<footer class="entry-footer">
		<?php echo tutorial_posted_footer(); ?>
	</footer>
</article>