<?php

/*
	Gallery Post Format
*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 tutorial-format-gallery'); ?>>
	<header class="entry-header text-center">
		<?php if(tutorial_get_attachment()): ?>

			<div id="post-gallery-<?php the_ID(); ?>" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
					<?php
						$attachments = tutorial_get_bs_slides(tutorial_get_attachment(7));
						foreach ($attachments as $attachment) {
					?>

						<div class="item <?php echo $attachment['class']; ?> background-image standard-featured" style="background-image: url(<?php echo $attachment['url']; ?>);">
							<?php if ($attachment['excerpt']) { ?>
								<div class="entry-excerpt image-caption">
									<p><?php echo $attachment['excerpt']; ?></p>
								</div>
							<?php } ?>
						</div>

					<?php } ?>
				</div>

				<a class="left carousel-control" href="#post-gallery-<?php the_ID(); ?>" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#post-gallery-<?php the_ID(); ?>" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>

		<?php endif; ?>

		<?php the_title('<h1 class="entry-title"><a href="'. esc_url(get_permalink()) .'" rel="bookmark">', '</a></h1>'); ?>

		<div class="entry-meta">
			<?php echo tutorial_posted_meta(); ?>
		</div>
	</header>

	<div class="entry-content">
		<div class="entry-excerpt">
			<?php the_excerpt(); ?>
		</div>

		<div class="button-container text-center">
			<a href="<?php the_permalink(); ?>" class="btn btn-tutorial"><?php _e('Read More'); ?></a>
		</div>
	</div>

	<footer class="entry-footer">
		<?php echo tutorial_posted_footer(); ?>
	</footer>
</article>