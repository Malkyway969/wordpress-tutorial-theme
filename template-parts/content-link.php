<?php

/*
	Link Post Format
*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 tutorial-format-link'); ?>>
	<header class="entry-header text-center">
		<?php
			$link = tutorial_grab_url();
 			the_title('<h1 class="entry-title"><a href="'. $link .'" target="_blank">', '<div class="link-icon"><span class="dashicons dashicons-admin-links"></span></div></a></h1>'); 
	 	?>
	</header>
</article>