<?php

/*
	Quote Post Format
*/

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-xs-12 tutorial-format-quote'); ?>>
	<header class="entry-header text-center">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				<h1 class="quote-content"><?php echo the_content(); ?></h1>		

				<?php the_title('<h2 class="quote-author">-', '-</h2>'); ?>
			</div>
		</div>
	</header>

	<footer class="entry-footer">
		<?php echo tutorial_posted_footer(); ?>
	</footer>
</article>