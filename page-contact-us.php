<?php
	$response = '';
    // if the submit button is clicked, send the email
    if ( isset( $_POST['submit'] ) ) {

        // sanitize form values
        $name    = sanitize_text_field( $_POST["full-name"] );
        $email   = sanitize_email( $_POST["email"] );
        $message = esc_textarea( $_POST["message"] );

        // get the blog administrator's email address
        $to = get_option( 'admin_email' );
        $subject = 'DigiCraft Request';

        $headers = "From: $name <$email>" . "\r\n";

        // If email has been process for sending, display a success message
        if ( wp_mail( $to, $subject, $message, $headers ) ) {
        	$response = 'Thanks for contacting DigiCraft, expect a repsonse soon';
        } else {
        	$response = 'It seems an error has occured';
        }
    }
?>

<?php 
	$fullwidthoption = get_option('viewwidth_header');
	$tilesaboveoption = get_option('tiles_above');

	if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
	    get_header();
	}
	else {
	    get_header('containedwidth');
	}
?>

<div class="container background-white">
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
			    <?php
				    // TO SHOW THE PAGE CONTENTS
				    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				        <div class="contact-page-content col-xs-12">
				            <?php the_content(); ?> <!-- Page Content -->
				        </div><!-- .entry-content-page -->
				        <div class="col-xs-12 col-md-6 col-md-offset-3 ">
				        	<div class="response"><?php echo $response ?></div>		
					        <form id="contactForm" action="<?php echo esc_url(the_permalink()); ?>" method="post" class="contact-form">
					        	<input class="form-name" type="text" name="full-name" placeholder="Full name" required>
					        	<input class="form-email" type="email" name="email" placeholder="Email" required>
					        	<textarea class="form-message" name="message" placeholder="Message" required></textarea>
					        	<input type="hidden" name="action" value="contact_form">
					        	<button type="submit" name="submit" class="btn form-btn">Submit <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
					        </form>
				        </div>
				    <?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
			</div>
			<?php if (isset($fullwidthoption) && ($fullwidthoption == 0)) : ?>
				<div class="row">
					<?php get_footer(); ?>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>

<?php if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
		get_footer();
	}  
?>