<?php 
	$fullwidthoption = get_option('viewwidth_header');
	$tilesaboveoption = get_option('tiles_above');

	if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
	    get_header();
	}
	else {
	    get_header('containedwidth');
	}
?>

<div class="container background-white">
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
			    <?php
				    // TO SHOW THE PAGE CONTENTS
				    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				        <div class="team-page-content col-xs-12">
				            <?php the_content(); ?> <!-- Page Content -->
				        </div><!-- .entry-content-page -->

				    <?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
			</div>
			<div class="row">
				<?php
					the_post();

					// Get 'team' posts
					$team_posts = get_posts( array(
						'post_type' => 'team',
						'posts_per_page' => -1, // Unlimited posts
						'orderby' => 'title', // Order alphabetically by name
					) );

					if ( $team_posts ):
				?>

					<?php 
						foreach ( $team_posts as $post ): 
						setup_postdata($post);
					?>
					<div class="col-xs-12 col-md-6 team-profile">
						<div class="row">
							<div class="col-xs-12 col-sm-4">
								<div class="team-picture"><?php echo the_post_thumbnail() ?></div>
							</div>
							<div class="col-xs-12 col-sm-8 team-info">
								<div class="row">
									<div class="col-xs-12 team-title">
										<?php echo the_title(); ?>
									</div>
									<div class="col-xs-12 team-position">
										<?php echo the_field('team_position'); ?>
									</div>
									<div class="col-xs-12 team-email">
										<a href="mailto:<?php echo the_field('team_email'); ?>"><?php echo the_field('team_email'); ?></a>
									</div>
									<div class="col-xs-12 team-phone">
										<?php echo the_field('team_phone'); ?>
									</div>
									<div class="col-xs-12 team-content">
										<?php echo $post->post_content; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<?php if (isset($fullwidthoption) && ($fullwidthoption == 0)) : ?>
				<div class="row">
					<?php get_footer(); ?>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>

<?php if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
		get_footer();
	}  
?>