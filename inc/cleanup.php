<?php

/*
	Remove all strings containing the current version of Wordpress
*/

//remove version string from js and css
function tutorial_remove_wp_version_strings($src) {
	global $wp_version;
	parse_str(parse_url($src, PHP_URL_QUERY), $query);
	if (!empty($query['ver']) && $query['ver'] === $wp_version) {
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}

add_filter('script_loader_src', 'tutorial_remove_wp_version_strings'); //filter to call, name of function to call
add_filter('style_loader_src', 'tutorial_remove_wp_version_strings');

//remove metatag generator from header
function tutorial_remove_meta_version() {
	return '';
}

add_filter('the_generator', 'tutorial_remove_meta_version');