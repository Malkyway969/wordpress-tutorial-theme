<?php

// $contact = get_option("activate_contact");
// if (isset($contact) && $contact == 1) {
// 	add_action("init", "tutorial_contact_custom_post_type");

// 	add_filter("manage_tutorial-contact_posts_columns", "tutorial_set_contact_columns");
// 	add_action("manage_tutorial-contact_posts_custom_column", "tutorial_contact_custom_column", 10, 2);

// 	add_action("add_meta_boxes", "tutorial_contact_add_meta_box");
// 	add_action("save_post", "tutorial_save_contact_email_data");
// }

// /*Contact Custom Post Type*/
// function tutorial_contact_custom_post_type() {
// 	$labels = array(
// 		"name" 				=> "Messages",
// 		"singular_name" 	=> "Message",
// 		"menu_name" 		=> "Messages",
// 		"name_admin_bar" 	=> "Message"
// 	);

// 	$args = array (
// 		"labels" 			=> $labels,
// 		"show_ui" 			=> true,
// 		"show_in_menu" 		=> true,
// 		"capability_type" 	=> "post",
// 		"hierarchical" 		=> false,
// 		"menu_position"		=> 26,
// 		"menu_icon"			=> "dashicons-email-alt",
// 		"supports"			=> array("title", "editor", "author")
// 	);

// 	register_post_type("tutorial-contact", $args);
// }

// function tutorial_set_contact_columns($columns) {
// 	$newColumns = array();
// 	$newColumns["title"] = "Full Name";
// 	$newColumns["message"] = "Message";
// 	$newColumns["email"] = "Email";
// 	$newColumns["date"] = "Date";
// 	return $newColumns;
// }

// function tutorial_contact_custom_column($column, $post_id) {
// 	switch($column){
// 		case "message" :
// 			echo get_the_excerpt();
// 			break;

// 		case "email" :
// 			$email = get_post_meta($post_id, "_contact_email_value_key", true);
// 			echo "<a href=\"mailto:".$email."\">".$email."</a>";
// 			break;
// 	}
// }

// /*Contact metaboxes*/
// function tutorial_contact_add_meta_box() {
// 	add_meta_box("contact_email", "User Email", "tutorial_contact_email", "tutorial-contact", "side"); // id, title, callback function, screen, context, priority, arguments array
// }

// function tutorial_contact_email($post) {
// 	wp_nonce_field("tutorial_save_contact_email_data", "tutorial_contact_email_meta_box_nonce"); //action, name, referer, echo(bool)

// 	$value = get_post_meta($post->ID, "_contact_email_value_key", true); //id, key, single(bool)

// 	echo "<label for=\"tutorial_contact_email_field\">User Email Address: </label>";
// 	echo "<input type=\"email\" id=\"tutorial_contact_email_field\" name=\"tutorial_contact_email_field\" value=\"". esc_attr($value) ."\" size=\"25\"/>";
// }

// function tutorial_save_contact_email_data($post_id) {
// 	if (!isset($_POST["tutorial_contact_email_meta_box_nonce"])) {
// 		return;
// 	}

// 	if (!wp_verify_nonce($_POST["tutorial_contact_email_meta_box_nonce"], "tutorial_save_contact_email_data")) {
// 		return;
// 	}

// 	if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
// 		return;
// 	}

// 	if (!current_user_can("edit_post", $post_id)) {
// 		return;
// 	}

// 	if (!isset($_POST["tutorial_contact_email_field"])) {
// 		return;
// 	}

// 	$contact_email = sanitize_text_field($_POST["tutorial_contact_email_field"]);

// 	update_post_meta($post_id, "_contact_email_value_key", $contact_email); //post_id, meta_key, meta_value
// }

/* Team Post Type */
function team_post_type() {
   
   // Labels
	$labels = array(
		'name' => _x("Team", "post type general name"),
		'singular_name' => _x("Team", "post type singular name"),
		'menu_name' => 'Team Profiles',
		'add_new' => _x("Add New", "team item"),
		'add_new_item' => __("Add New Profile"),
		'edit_item' => __("Edit Profile"),
		'new_item' => __("New Profile"),
		'view_item' => __("View Profile"),
		'search_items' => __("Search Profiles"),
		'not_found' =>  __("No Profiles Found"),
		'not_found_in_trash' => __("No Profiles Found in Trash"),
		'parent_item_colon' => ''
	);
	
	// Register post type
	register_post_type('team' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-admin-users',
		'rewrite' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	) );
}
add_action( 'init', 'team_post_type', 0 );

/* Portfolio Post Type */
function portfolio_post_type() {
   
   // Labels
	$labels = array(
		'name' => _x("Portfolio", "post type general name"),
		'singular_name' => _x("Portfolio", "post type singular name"),
		'menu_name' => 'Portfolio Details',
		'add_new' => _x("Add New", "portfolio item"),
		'add_new_item' => __("Add New Portfolio Piece"),
		'edit_item' => __("Edit Portfolio Piece"),
		'new_item' => __("New Portfolio Piece"),
		'view_item' => __("View Portfolio Piece"),
		'search_items' => __("Search Portfolio Piece"),
		'not_found' =>  __("No Portfolio Piece Found"),
		'not_found_in_trash' => __("No Portfolio Piece Found in Trash"),
		'parent_item_colon' => ''
	);
	
	// Register post type
	register_post_type('portfolio' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-awards',
		'rewrite' => false,
		'supports' => array('title', 'editor', 'thumbnail')
	) );
}
add_action( 'init', 'portfolio_post_type', 0 );