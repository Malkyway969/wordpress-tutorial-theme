<h1>Tutorial Theme Options</h1>
<?php settings_errors(); ?>

<?php
	$picture = esc_attr(get_option("profile_picture"));
?>
<div class="tutorial-sidebar-preview">
	<div class="tutorial-sidebar">
		<div class="image-container">
			<div id="profile-picture-preview" class="profile-picture" style="background-image: url(<?php print $picture; ?>);"></div>
		</div>
	</div>
</div>

<form method="post" action="options.php" class="tutorial-general-form">
	<?php settings_fields("tutorial-theme-options"); ?>
	<?php do_settings_sections("malcolm_tutorial_settings"); ?>
	<?php submit_button(); ?>
</form>