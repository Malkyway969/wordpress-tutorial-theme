<h1>Tutorial Theme Custom CSS</h1>
<?php settings_errors(); ?>

<form id="save-custom-css-form" method="post" action="options.php" class="tutorial-general-form">
	<?php settings_fields("tutorial-custom-css-options"); ?>
	<?php do_settings_sections("malcolm_tutorial_css"); ?>
	<?php submit_button(); ?>
</form>