<?php
//Adds pages to the sidebar in admin area
function tutorial_add_admin_page() {
	add_menu_page('Tutorial Theme Settings', 'Tutorial Theme', 'manage_options', 'malcolm_tutorial', 'tutorial_theme_options_page', null, 110); //Name of page, page's name is sidebar, who can access, unique slug, call function for html, custom icon url, position of menu(where in the order it appears)

	// add_submenu_page("malcolm_tutorial", "Sidebar Settings", "Sidebar", "manage_options", "malcolm_tutorial", "tutorial_theme_create_page"); //slug of parent page, name of page, submenu name, privileges, submenu slug, function for html

	add_submenu_page("malcolm_tutorial", "Tutorial Theme Options", "Theme Options", "manage_options", "malcolm_tutorial", "tutorial_theme_options_page");

	add_submenu_page("malcolm_tutorial", "Tutorial Custom Css", "Custom CSS", "manage_options", "malcolm_tutorial_css", "tutorial_theme_css_page");

	add_action("admin_init", "tutorial_custom_settings");
}

add_action( 'admin_menu', 'tutorial_add_admin_page' ); //hook to when admin page is loaded, function to be called

//Functions that call the pages content
// function tutorial_theme_create_page() {
// 	require_once( get_template_directory() . "/inc/templates/tutorial-admin.php");
// }

function tutorial_theme_options_page() {
	require_once( get_template_directory() . "/inc/templates/tutorial-theme-options.php");
}

function tutorial_theme_css_page() {
	require_once( get_template_directory() . "/inc/templates/tutorial-theme-css.php");
}

//Function that calls the settings sections and fields for each page
function tutorial_custom_settings() {
	//Sidebar options
	 //initialises setting. id of page, id of setting
	// register_setting("tutorial-settings-group", "first_name");
	// register_setting("tutorial-settings-group", "last_name");
	// register_setting("tutorial-settings-group", "user_description");
	// register_setting("tutorial-settings-group", "twitter", "sanitize_twitter_handle");
	// register_setting("tutorial-settings-group", "facebook");
	// register_setting("tutorial-settings-group", "gplus");
	// add_settings_section("tutorial-sidebar-options", "Sidebar Options", "tutorial_sidebar_options", "malcolm_tutorial");// initialises a new section. id of section, name of section, callback function, id of page it goes on
	// //initialises the field for a setting. id of setting, label of setting, callback function, id of page its on, section it is going in
	// add_settings_field("sidebar-name", "Name", "tutorial_sidebar_name", "malcolm_tutorial", "tutorial-sidebar-options");
	// add_settings_field("sidebar-description", "Description", "tutorial_sidebar_description", "malcolm_tutorial", "tutorial-sidebar-options");
	// add_settings_field("sidebar-twitter", "Twitter", "tutorial_sidebar_twitter", "malcolm_tutorial", "tutorial-sidebar-options");
	// add_settings_field("sidebar-facebook", "Facebook", "tutorial_sidebar_facebook", "malcolm_tutorial", "tutorial-sidebar-options");
	// add_settings_field("sidebar-gplus", "Google+", "tutorial_sidebar_gplus", "malcolm_tutorial", "tutorial-sidebar-options");

	//Theme Options
	register_setting("tutorial-theme-options", "profile_picture");
	register_setting("tutorial-theme-options", "phone_number");
	register_setting("tutorial-theme-options", "post_formats");
	register_setting("tutorial-theme-options", "custom_header");
	register_setting("tutorial-theme-options", "custom_background");
	register_setting("tutorial-theme-options", "viewheight_header");
	register_setting("tutorial-theme-options", "viewwidth_header");
	register_setting("tutorial-theme-options", "tiles_above");
	add_settings_section("tutorial-theme-options", "Theme Options", "tutorial_theme_options", "malcolm_tutorial_settings");
	add_settings_field("sidebar-picture", "Logo", "tutorial_sidebar_picture", "malcolm_tutorial_settings", "tutorial-theme-options");
	add_settings_field("phone-number", "Phone Number", "tutorial_phone_number", "malcolm_tutorial_settings", "tutorial-theme-options");
	add_settings_field("post-formats", "Post Formats", "tutorial_post_formats", "malcolm_tutorial_settings", "tutorial-theme-options");
	add_settings_field("custom-header", "Custom Header", "tutorial_custom_header", "malcolm_tutorial_settings", "tutorial-theme-options");
	add_settings_field("custom-background", "Custom Background", "tutorial_custom_background", "malcolm_tutorial_settings", "tutorial-theme-options");
	add_settings_field("viewheight-header", "Fill Viewport Height Header", "tutorial_viewheight_header", "malcolm_tutorial_settings", "tutorial-theme-options");
	add_settings_field("viewwidth-header", "Full Width Header and Footer", "tutorial_viewwidth_header", "malcolm_tutorial_settings", "tutorial-theme-options");
	add_settings_field("tiles-above", "Front Page Tiles above content", "tutorial_tiles_above", "malcolm_tutorial_settings", "tutorial-theme-options");

	//Custom CSS
	register_setting("tutorial-custom-css-options", "tutorial_css", "tutorial_sanitize_custom_css");
	add_settings_section("tutorial-custom-css-section", "Custom CSS", "tutorial_custom_css_section_callback", "malcolm_tutorial_css");
	add_settings_field("custom-css", "Insert your Custom CSS", "tutorial_custom_css_callback", "malcolm_tutorial_css", "tutorial-custom-css-section");
}

//Callback functions
function tutorial_sidebar_options() {
	echo "Customise Sidebar Information";
}

function tutorial_sidebar_picture() {
	$picture = esc_attr(get_option("profile_picture"));
	if (empty($picture)) {
		echo "<input type=\"button\" class=\"button button-secondary\" value=\"Upload Logo\" id=\"upload-button\" /><input type=\"hidden\" id=\"profile-picture\" name=\"profile_picture\" value=\"" . $picture ."\" />";
	}
	else {
		echo "<input type=\"button\" class=\"button button-secondary\" value=\"Replace Logo\" id=\"upload-button\" /><input type=\"hidden\" id=\"profile-picture\" name=\"profile_picture\" value=\"" . $picture ."\" /> <input type=\"button\" class=\"button button-secondary\" value=\"Remove\" id=\"remove-picture\"/> ";
	}
}

function tutorial_sidebar_name() {
	$firstName = esc_attr(get_option("first_name"));
	$lastName = esc_attr(get_option("last_name"));
	echo "<input type=\"text\" name=\"first_name\" value=\"" . $firstName ."\" placeholder=\"First Name\" /> <input type=\"text\" name=\"last_name\" value=\"" . $lastName ."\" placeholder=\"Last Name\" />";
}

function tutorial_sidebar_description() {
	$description = esc_attr(get_option("user_description"));
	echo "<input type=\"text\" name=\"user_description\" value=\"" . $description ."\" placeholder=\"A little bit about yourself\" />";
}

function tutorial_sidebar_twitter() {
	$twitter = esc_attr(get_option("twitter"));
	echo "<input type=\"text\" name=\"twitter\" value=\"" . $twitter ."\" placeholder=\"Twitter Handle\" /><p class=\"description\">Input yout Twitter username without the @ character.</p>";
}

function tutorial_sidebar_facebook() {
	$facebook = esc_attr(get_option("facebook"));
	echo "<input type=\"text\" name=\"facebook\" value=\"" . $facebook ."\" placeholder=\"Facebook Handle\" />";
}

function tutorial_sidebar_gplus() {
	$gplus = esc_attr(get_option("gplus"));
	echo "<input type=\"text\" name=\"gplus\" value=\"" . $gplus ."\" placeholder=\"Google+ Handle\" />";
}

function sanitize_twitter_handle($input) {
	$output = sanitize_text_field($input);
	$output = str_replace("@", "", $output);
	return $output;
}

function tutorial_theme_options() {
	echo "Activate and Deactivate specific Theme Options";
}

function tutorial_phone_number() {
	$phoneNumber = esc_attr(get_option("phone_number"));
	echo "<input type=\"text\" name=\"phone_number\" value=\"" . $phoneNumber ."\" placeholder=\"Your Number\" />";
}

function tutorial_post_formats() {
	$options = get_option("post_formats");
	$formats = array("aside", "gallery", "link", "image", "quote", "status", "video", "audio", "chat");
	$output = "";
	foreach($formats as $format) {
		$checked = (isset($options[$format]) && $options[$format] == 1 ? "checked" : "");
		$output .= "<label><input type=\"checkbox\" id=\"". $format ."\" name=\"post_formats[". $format ."]\" value=\"1\" ". $checked ."/>". $format ."<br /></label>";
	}
	echo $output;
}

function tutorial_custom_header() {
	$options = get_option("custom_header");
	$checked = (isset($options) && $options == 1 ? "checked" : "");
	echo "<label><input type=\"checkbox\" id=\"custom_header\" name=\"custom_header\" value=\"1\" ". $checked ."/> Activate Custom Header</label>";
}

function tutorial_viewheight_header() {
	$options = get_option("viewheight_header");
	$checked = (isset($options) && $options == 1 ? "checked" : "");
	echo "<label><input type=\"checkbox\" id=\"viewheight_header\" name=\"viewheight_header\" value=\"1\" ". $checked ."/> Full Height Header on Front Page?</label>";
}

function tutorial_viewwidth_header() {
	$options = get_option("viewwidth_header");
	$checked = (isset($options) && $options == 1 ? "checked" : "");
	echo "<label><input type=\"checkbox\" id=\"viewwidth_header\" name=\"viewwidth_header\" value=\"1\" ". $checked ."/> Full Width Header and Footer?</label>";
}

function tutorial_tiles_above() {
	$options = get_option("tiles_above");
	$checked = (isset($options) && $options == 1 ? "checked" : "");
	echo "<label><input type=\"checkbox\" id=\"tiles_above\" name=\"tiles_above\" value=\"1\" ". $checked ."/> Tiles will display above content when ticked</label>";
}

function tutorial_custom_background() {
	$options = get_option("custom_background");
	$checked = (isset($options) && $options == 1 ? "checked" : "");
	echo "<label><input type=\"checkbox\" id=\"custom_background\" name=\"custom_background\" value=\"1\" ". $checked ."/> Activate Custom Background</label>";
}

// function tutorial_activate_contact() {
// 	$options = get_option("activate_contact");
// 	$checked = (isset($options) && $options == 1 ? "checked" : "");
// 	echo "<label><input type=\"checkbox\" id=\"activate_contact\" name=\"activate_contact\" value=\"1\" ". $checked ."/> Activate Contact Form</label>";
// }

function tutorial_custom_css_section_callback() {
	echo "Customise your Tutorial Theme with your own CSS";
}

function tutorial_custom_css_callback() {
	$css = get_option("tutorial_css");
	if (empty($css)) {
		$css = '/*Tutorial Theme Custom CSS*/';
	}
	else {
		$css = $css;
	}

	echo "<div id=\"customCss\" >".$css."</div><textarea id=\"tutorial_css\" name=\"tutorial_css\" style=\"display:none;visibility:hidden;\" >".$css."</textarea>";
}

function tutorial_sanitize_custom_css($input) {
	$output = esc_textarea($input);
	return $output;
}