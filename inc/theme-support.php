<?php

//Theme support options that come with wordpress

$options = get_option("post_formats");
foreach ($options as $option => $value) {
  $output[] = $option;
}
if (!empty($options)) {
	add_theme_support("post-formats", $output);
}

$header = get_option("custom_header");
if (isset($header) && $header == 1) {
	add_theme_support("custom-header");
}

$background = get_option("custom_background");
if (isset($background) && $background == 1) {
	add_theme_support("custom-background");
}

add_theme_support( 'custom-logo' );

add_theme_support('post-thumbnails');

/*Activate nav*/
function tutorial_register_nav_menu() {
	register_nav_menu('primary', 'Header Navigation Menu'); //location, description
}
add_action('after_setup_theme', 'tutorial_register_nav_menu');

/**/

function tutorial_posted_meta() {
	$posted_on = human_time_diff(get_the_time('U'), current_time('timestamp'));
	$categories = get_the_category();
	$separator = ', ';
	$output = '';
	$i = 1;

	if(!empty($categories)) {
		foreach($categories as $category) {
			if($i > 1) {
				$output .= $separator;
			}
			$output .=  '<a href="'. esc_url(get_category_link($category->term_id)) .'" alt="'. esc_attr('View all posts in%s', $category->name) .'">'. esc_html($category->name) .'</a>';
			$i++;
		}
	}
	return '<span class="posted-on">Posted <a href="'.esc_url(get_permalink()).'">'. $posted_on .'</a> ago</span> / <span class="posted-in">'. $output .'</span>';
}

function tutorial_posted_footer() {
	$comments_num = get_comments_number();
	if (comments_open()) {
		if ($comments_num == 0) {
			$comments = __('No Comments yet');
		}
		else if ($comments_num > 1) {
			$comments = $comments_num . __(' Comments');
		}
		else {
			$comments = __('1 Comment');
		}
		$comments = '<a class="comments-link" href="'. get_comments_link() .'">'. $comments .'</a> <span class="dashicons dashicons-testimonial"></span>';
	}
	else {
		$comments = __('Comments are disabled');
	}

	return '<div class="post-footer-container"><div class="row"><div class="col-xs-12 col-sm-6">'. get_the_tag_list('<div class="tags-list"><span class="dashicons dashicons-tag"></span> ', ' ', '</div>') .'</div><div class="col-xs-12 col-sm-6 text-right">'. $comments .'</div></div></div>';
}

function tutorial_get_attachment($num = 1) {
	$output = '';
	if (has_post_thumbnail() && $num == 1) {
		$output = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
	}
	else {
		$attachments = get_posts(array(
			'post_type' => 'attachment',
			'posts_per_page' => $num,
			'post_parent' => get_the_ID()
		));

		if ($attachments && $num == 1) {
			foreach($attachments as $attachment) {
				$output = wp_get_attachment_url($attachment->ID);
			}
		}
		else if ($attachments && $num > 1) {
			$output = $attachments;
		}
	}

	wp_reset_postdata();
	return $output;
}

function tutorial_get_media($type = array()) {
	$content = do_shortcode(apply_filters('the_content', get_the_content()));
	$embed = get_media_embedded_in_content($content, $type);

	if (in_array('audio', $type)) {
		$output = str_replace('?visual=true', '?visual=false', $embed[0]);
	}
	else {
		$output = $embed[0];
	}
	return $output;
}

function tutorial_get_bs_slides($attachments) {
	$i = 0;
	$output = array();

	foreach ($attachments as $attachment) {
		$active = ($i == 0 ? 'active' : '');

		$output[$i] = array(
			'class'		=> $active,
			'url'		=> wp_get_attachment_url($attachments[$i]->ID),
			'excerpt'	=> $attachments[$i]->post_excerpt,
		);

		$i++;
	}

	return $output;
}

function tutorial_grab_url() {
	if( !preg_match('/<a\s[^>]*?href=[\'"](.+?)[\'"]/i', get_the_content(), $links)) {
		return false;
	}

	return esc_url_raw($links[1]);
}

//get excerpt by id
function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = ($the_post ? $the_post->post_content : null); //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 35; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '…');
        $the_excerpt = implode(' ', $words);
    endif;

    return $the_excerpt;
}