<?php

function tutorial_load_admin_scripts($hook) {
	if ("toplevel_page_malcolm_tutorial" == $hook) {
		wp_register_style("tutorial_admin", get_template_directory_uri() . "/css/tutorial_admin.css", array(), "1.0.0", "all");
		wp_enqueue_style("tutorial_admin");

		wp_enqueue_media();

		wp_register_script("tutorial-admin-script", get_template_directory_uri() . "/js/tutorial-admin.js", array("jquery"), "1.0.0", true);
		wp_enqueue_script("tutorial-admin-script");
	}
	else if ("tutorial-theme_page_malcolm_tutorial_css" == $hook) {
		wp_enqueue_style( 'ace', get_template_directory_uri() . '/css/tutorial_ace.css', array(), '1.0.0', 'all' );
		
		wp_enqueue_script( 'ace', get_template_directory_uri() . '/js/ace/ace.js', array('jquery'), '1.2.6', true );
		wp_enqueue_script( 'tutorial-custom-css-script', get_template_directory_uri() . '/js/tutorial-custom_css.js', array('jquery'), '1.0.0', true );
	}
	else {
		return;
	}
}

add_action("admin_enqueue_scripts", "tutorial_load_admin_scripts");

/*
	Front end functions
*/

function tutorial_load_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css', array(), '3.3.7', 'all' );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), '1.0.0', 'all' );
	wp_enqueue_style( 'slabo-raleway-ranga', 'https://fonts.googleapis.com/css?family=Raleway|Ranga|Slabo+27px');
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/bootstrap/js/bootstrap.min.js', array('jquery'), '3.3.7', true );
	wp_enqueue_script( 'tutorial', get_template_directory_uri() . '/js/tutorial.js', array('jquery'), '1.0.0', true );
}

add_action("wp_enqueue_scripts", "tutorial_load_scripts");