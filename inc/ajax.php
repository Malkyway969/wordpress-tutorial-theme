<?php

add_action('wp_ajax_nopriv_tutorial_load_more', 'tutorial_load_more');
add_action('wp_ajax_tutorial_load_more', 'tutorial_load_more');

function tutorial_load_more() {
	$paged = $_POST["page"]+1;

	$query = new WP_Query(array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'paged' => $paged
	));

	if ($query->have_posts()):

		echo '<div class="page-limit" data-page=" '.get_site_url().'/page/'.$paged.'">';

		while ($query->have_posts()): $query->the_post();
			get_template_part('template-parts/content', get_post_format());
		endwhile;

		echo '</div>';
	endif;

	wp_reset_postdata();

	wp_die();
}