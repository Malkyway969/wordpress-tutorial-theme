<?php 

	$fullheightoption = get_option('viewheight_header');
	$fullwidthoption = get_option('viewwidth_header');
	$tilesaboveoption = get_option('tiles_above');

	if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
		if(is_front_page() && isset($fullheightoption) && ($fullheightoption == 1))
		{
		    get_header('home');
		}
		else
		{
		    get_header();
		}
	}
	else {
		if(is_front_page() && isset($fullheightoption) && ($fullheightoption == 1))
		{
		    get_header('home');
		}
		else
		{
		    get_header('containedwidth');
		}
	}


?>


<div class="container background-white">
	<div class="row">
		<div class="col-xs-12">
			<?php if (isset($tilesaboveoption) && ($tilesaboveoption == 1)) : ?>
				<div class="row" style="margin-top: 20px;">
					<?php $the_query = new WP_Query( array('post_type' => 'post', 'showposts' => '3', 'category_name' => 'front-page' )); ?>

					<?php while ($the_query -> have_posts()) : $the_query -> the_post(); setup_postdata($post); ?>
						<div class="col-xs-12 col-sm-4">								
							<div class="tileImage"><?php echo the_post_thumbnail(); ?></div>
							<div class="tileContent"><?php echo $post->post_content; ?><i class="fa fa-arrow-right" aria-hidden="true"></i></div>						
						</div>
					<?php
					endwhile;
					wp_reset_postdata();
					?>
				</div>
			<?php endif ?>
			<div class="row">
			    <?php
				    // TO SHOW THE PAGE CONTENTS
				    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				        <div class="entry-content col-xs-12">
				            <?php the_content(); ?> <!-- Page Content -->
				        </div><!-- .entry-content-page -->

				    <?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
			</div>
			<?php if (isset($tilesaboveoption) && ($tilesaboveoption == 0)) : ?>
				<div class="row">
					<?php $the_query = new WP_Query( array('post_type' => 'post', 'showposts' => '3', 'category_name' => 'front-page' )); ?>

					<?php while ($the_query -> have_posts()) : $the_query -> the_post(); setup_postdata($post); ?>
						<div class="col-xs-12 col-sm-4">								
							<div class="tileImage"><?php echo the_post_thumbnail(); ?></div>
							<div class="tileContent"><?php echo $post->post_content; ?><i class="fa fa-arrow-right" aria-hidden="true"></i></div>						
						</div>
					<?php
					endwhile;
					wp_reset_postdata();
					?>
				</div>
			<?php endif ?>
			<?php if (isset($fullwidthoption) && ($fullwidthoption == 0)) : ?>
				<div class="row">
					<?php get_footer(); ?>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>

<?php if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
		get_footer();
	}  
?>