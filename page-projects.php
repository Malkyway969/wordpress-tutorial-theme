<?php 
	$fullwidthoption = get_option('viewwidth_header');
	$tilesaboveoption = get_option('tiles_above');

	if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
	    get_header();
	}
	else {
	    get_header('containedwidth');
	}
?>

<div class="container background-white">
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
			    <?php
				    // TO SHOW THE PAGE CONTENTS
				    while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
				        <div class="portfolio-page-content col-xs-12">
				            <?php the_content(); ?> <!-- Page Content -->
				        </div><!-- .entry-content-page -->

				    <?php
				    endwhile; //resetting the page loop
				    wp_reset_query(); //resetting the page query
				    ?>
			</div>
			<div class="row">
				<?php
					the_post();

					// Get 'team' posts
					$portfolio_posts = get_posts( array(
						'post_type' => 'portfolio',
						'posts_per_page' => -1, // Unlimited posts
						'orderby' => 'date', // Order alphabetically by date
					) );

					if ( $portfolio_posts ):
				?>

					<?php 
						foreach ( $portfolio_posts as $post ): 
						setup_postdata($post);
					?>
					<div class="col-xs-12 col-sm-6 col-md-4 portfolio piece">
						<div class="portfolio-image"><a href="<?php echo get_field('link_to_website'); ?>" target="_blank"><?php the_post_thumbnail(); ?><div class="portfolio-mask"></div><div class="portfolio-desktop-title hidden-xs hidden-sm hidden-md"><?php the_title(); ?></div></a></div>						
						<div class="portfolio-title hidden-lg"><?php the_title(); ?></div>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<?php if (isset($fullwidthoption) && ($fullwidthoption == 0)) : ?>
				<div class="row">
					<?php get_footer(); ?>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>

<?php if (isset($fullwidthoption) && ($fullwidthoption == 1)) {
		get_footer();
	}  
?>