<?php wp_footer(); ?>

<footer class="pageFooter">
	<div class="container">
		<div class="row">
			<div id="footer-sidebar">
				<div id="footer-sidebar1"  class="col-xs-12">
					<?php
						if(is_active_sidebar('left-widget-area')){
							dynamic_sidebar('left-widget-area');
						}
					?>
				</div>
				<div id="footer-sidebar2" class="col-xs-12">
					<?php
						if(is_active_sidebar('right-widget-area')){
							dynamic_sidebar('right-widget-area');
						}
					?>
				</div>
			</div>
		</div>
	</div>
</footer>

</div><!-- row -->
</div><!-- container-fluid -->
</body>
</html>