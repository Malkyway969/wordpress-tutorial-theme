<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php if (is_singular() && pings_open(get_queried_object())): ?>
			<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php endif; ?>
		<title><?php bloginfo("name"); wp_title(); ?></title>
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
	<div class="container-fluid">
		<div class="row">
			<!-- <div class="container">
				<div class="row">
					<div id="phone-number" class="col-xs-6">
						<h3><i class="fa fa-mobile hidden-xs" aria-hidden="true"></i> </i><?php //echo get_option( 'phone_number' ); ?></h3>
					</div>	
					<div class="col-xs-6 socialMediaIcons">
						<?php //if (function_exists("DISPLAY_ACURAX_ICONS")) { DISPLAY_ACURAX_ICONS(); } ?>
					</div>
				</div>
			</div> -->

			<header class="header-container container" style="background-image: url(<?php header_image(); ?>)">
					<div class="row">
						<div class="header-content col-xs-12 col-sm-5 col-md-6">
							<?php the_custom_logo(); ?>
							<?php if (!has_custom_logo()) : ?>
								<h1 class="site-title">
									<a href="<?php echo home_url(); ?>"><?php bloginfo("name"); ?></a>
								</h1>
								<h2 class="site-description">
									<?php bloginfo("description"); ?>
								</h2>
							<?php endif ?>
						</div>
						<div class="nav-container col-xs-12 col-sm-7 col-md-6">
							<nav class="navbar navbar-default navbar-tutorial" role="navigation">
							    <!-- Brand and toggle get grouped for better mobile display -->
							    <div class="navbar-header">
							        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							            <span class="sr-only">Toggle navigation</span>
							            <span class="icon-bar"></span>
							            <span class="icon-bar"></span>
							            <span class="icon-bar"></span>
							        </button>
							    </div>

							    <!-- Collect the nav links, forms, and other content for toggling -->
							    <div class="collapse navbar-collapse navbar-ex1-collapse">
							        <?php 
										wp_nav_menu(array(
											'theme_location' 	=> 'primary',
											'container' 		=> false,
											'menu_class'		=> 'nav navbar-nav',
											'walker'			=> new Tutorial_Walker_Nav_Primary()
										));
									?>
							    </div><!-- /.navbar-collapse --> 
							</nav>
						</div>
					</div>
			</header>
